import 'package:flutter/material.dart';

class ThemeNotifier with ChangeNotifier {
  ThemeMode _themeMode;
  int _themeIndex;

  ThemeNotifier(this._themeMode, this._themeIndex);

  getThemeMode() => _themeMode;
  int getThemeIndex() => _themeIndex;

  setThemeMode(ThemeMode mode) async {
    _themeMode = mode;
    notifyListeners();
  }

  setThemeIndex(int _index) async {
    _themeIndex = _index;

    if (_index == 0) {
      setThemeMode(ThemeMode.light);
    } else if (_index == 1) {
      setThemeMode(ThemeMode.dark);
    } else if (_index == 2) {
      setThemeMode(ThemeMode.system);
    }
    notifyListeners();
  }
}
