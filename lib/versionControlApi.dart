import 'dart:convert';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ConfigData/config.dart';

class VersionControlApi {
  static bool isLoaded = false;
  static String responseBody = "";

  Future<bool> checkVersion() async {
    if (!isLoaded) {
      final prefs = await SharedPreferences.getInstance();
      if (prefs.containsKey("versionControlResponseJSON")) {
        //read from local storage
        responseBody = (prefs.getString('versionControlResponseJSON') ?? "");
      }

      try {
        Map<String, dynamic> requestBody = {
          'code': 'versionCheck',
          'appBuildNumber': ConfigApp.buildNumber,
          'appBuildVersion': ConfigApp.buildVersion,
        };

        //for testing: https://jsonplaceholder.typicode.com/posts
        await post(
          Uri.parse(
              "https://hosting.swibble.net/konrats-welt/api/app/versionControl/"),
          body: json.encode(requestBody),
        ).then((value) {
          responseBody = value.body;

          //write to local storage
          prefs.setString('versionControlResponseJSON', responseBody);

          return value;
        });
      } catch (e) {
        print(e);
      }

      isLoaded = true;
    }

    Map<String, dynamic> jsonData = jsonDecode(responseBody);
    ConfigApi.newestBuildNumber = jsonData["newestBuildNumber"];
    ConfigApi.allUserBanned = jsonData["allUserBanned"];
    ConfigApi.allUserBannedReason = jsonData["allUserBannedReason"];
    ConfigApi.forceUpdateUnderBuildNumber =
        jsonData["forceUpdateUnderBuildNumber"];
    ConfigApi.forceUpdateForBuildNumbers =
        jsonData["forceUpdateForBuildNumbers"];
    ConfigApi.updateInformation = jsonData["updateInformation"];

    ConfigApi.needUpdate = needUpdate(jsonData);

    bool showErrorPage = false;
    if (ConfigApi.allUserBanned || ConfigApi.needUpdate) {
      showErrorPage = true;
    }
    return showErrorPage;
  }

  bool needUpdate(jsonData) {
    bool needUpdate = false;

    if (ConfigApi.forceUpdateUnderBuildNumber != null &&
        ConfigApp.buildNumber < ConfigApi.forceUpdateUnderBuildNumber) {
      needUpdate = true;
    }
    if (ConfigApi.forceUpdateForBuildNumbers != null &&
        ConfigApi.forceUpdateForBuildNumbers!.contains(ConfigApp.buildNumber)) {
      needUpdate = true;
    }
    return needUpdate;
  }
}
