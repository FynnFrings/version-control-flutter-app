import 'package:flutter/material.dart';

class AppTheme {
  get darkTheme => ThemeData(
        primaryTextTheme: TextTheme(
          subtitle2: TextStyle(color: Colors.grey[100]),
          headline6: TextStyle(color: Colors.white),
        ),
        primarySwatch: Colors.amber,
        primaryColor: Colors.white,
        shadowColor: Colors.black,
        scaffoldBackgroundColor: Color(0xff2d2d2d),
        appBarTheme: AppBarTheme(
          brightness: Brightness.dark,
          color: Color(0xff202020),
        ),

        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: Colors.grey[400]),
          labelStyle: TextStyle(color: Colors.white),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Color(0xff202020),
          selectedIconTheme: IconThemeData(color: Color(0xfffac520)),
          selectedLabelStyle: TextStyle(
            color: Colors.white,
          ),
        ),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.white),
          bodyText2: TextStyle(color: Colors.grey[200]),
          subtitle1: TextStyle(color: Colors.grey[200]),
          subtitle2: TextStyle(color: Colors.grey[300]),
          headline1: TextStyle(color: Colors.white),
          headline2: TextStyle(color: Colors.grey[200]),
          headline6: TextStyle(color: Colors.white),
          headline5: TextStyle(color: Color(0xff40A0F3)), //blue[400]
          headline4: TextStyle(color: Color(0xfffac520)), //d1b738
        ),
        brightness: Brightness.dark,
        canvasColor: Color(0xff353535),
        accentColor: Colors.white,
        primaryIconTheme: IconThemeData(color: Colors.white),
        //accentIconTheme: IconThemeData(color: Colors.white),
        iconTheme: IconThemeData(color: Colors.white),
        buttonColor: Color(0xff212121),

        floatingActionButtonTheme:
            FloatingActionButtonThemeData(backgroundColor: Color(0xfffac520)),
        cardColor: Color(0xff404040),
      );

  get lightTheme => ThemeData(
        //fontFamily: ConfigApp.font,
        //primarySwatch: Colors.grey,
        primaryColor: Color(0xff005A87),
        accentColor: Color(0xffe50051),
        shadowColor: Colors.black,
        scaffoldBackgroundColor: Color(0xfff5f5fa),
        appBarTheme: AppBarTheme(
          brightness: Brightness.light,
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: Colors.grey[500]),
          labelStyle: TextStyle(color: Colors.white),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Colors.white,
          selectedIconTheme: IconThemeData(color: Color(0xff005A87)),
          selectedLabelStyle: TextStyle(
            color: Colors.black,
          ),
        ),
        primaryTextTheme: TextTheme(
          subtitle2: TextStyle(color: Colors.grey[900]),
          headline6: TextStyle(color: Colors.black),
        ),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.black),
          bodyText2: TextStyle(color: Colors.grey[900]),
          subtitle1: TextStyle(color: Colors.grey[800]),
          subtitle2: TextStyle(color: Colors.grey[700]),
          headline1: TextStyle(color: Colors.black),
          headline2: TextStyle(color: Colors.grey[700]),
          headline6: TextStyle(color: Colors.grey[900]),
          headline5: TextStyle(color: Colors.blue[400]),
          headline4: TextStyle(color: Colors.blue[800]),
        ),
        canvasColor: Color(0xfff6f6f6),
        brightness: Brightness.light,
        primaryIconTheme: IconThemeData(color: Colors.black),
        //accentIconTheme: IconThemeData(color: Colors.black),
        iconTheme: IconThemeData(color: Colors.black),
        buttonColor: Colors.grey[400],
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: Color(0xff005A87), //004383
        ),
        cardColor: Color(0xfffdfdfd),
      );
}
