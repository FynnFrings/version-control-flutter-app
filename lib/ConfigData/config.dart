class ConfigCompany {
  //Company Name
  static String companyName = 'Company Name';
}

class ConfigApp {
  //App Name
  static String appName = "Version Control";

  //Build Version
  static int buildNumber = 5;
  static String buildVersion = '0.0.1';

  //Fonts
  static String font = 'Roboto';
}

class ConfigApi {
  //News Banner
  static bool showNewsBanner = true;

  //Version Control
  static int? newestBuildNumber;
  static bool allUserBanned = false;
  static String? allUserBannedReason;
  static int forceUpdateUnderBuildNumber = 0;
  static List<dynamic>? forceUpdateForBuildNumbers;
  static String? updateInformation;
  static bool needUpdate = false;
}
