import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:versioncontrol_app/versionCheckPage.dart';

import 'ConfigData/Theme/appTheme.dart';
import 'ConfigData/config.dart';
import 'Provider/themeNotifier.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeNotifier>(
          create: (_) => ThemeNotifier(ThemeMode.light, 0),
        ),
      ],
      child: new MaterialAppWithTheme(),
    );
  }
}

class MaterialAppWithTheme extends StatefulWidget {
  @override
  _MaterialAppWithThemeState createState() => _MaterialAppWithThemeState();
}

class _MaterialAppWithThemeState extends State<MaterialAppWithTheme> {
  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeNotifier>(context);

    return MaterialApp(
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: child != null ? child : Container(),
          ),
        );
      },
      debugShowCheckedModeBanner: false,
      title: ConfigApp.appName,
      themeMode: theme.getThemeMode(),
      theme: AppTheme().lightTheme,
      darkTheme: AppTheme().darkTheme,
      home: VersionCheckpage(),
    );
  }
}
