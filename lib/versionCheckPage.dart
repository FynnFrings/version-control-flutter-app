import 'package:flutter/material.dart';
import 'package:versioncontrol_app/versionControlApi.dart';

import 'ConfigData/config.dart';

class VersionCheckpage extends StatefulWidget {
  const VersionCheckpage({Key? key}) : super(key: key);

  @override
  _VersionCheckpageState createState() => _VersionCheckpageState();
}

class _VersionCheckpageState extends State<VersionCheckpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    width: 100,
                    color: ConfigApi.allUserBanned ? Colors.red : Colors.green,
                    child: Center(child: Text("Blocked")),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: ConfigApi.needUpdate ? Colors.orange : Colors.green,
                    child: Center(child: Text("Update")),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Text("Current App Version: ${ConfigApp.buildVersion}"),
              Text("Current Build Number: ${ConfigApp.buildNumber}"),
              SizedBox(
                height: 15,
              ),
              Text("newest Build Number: ${ConfigApi.newestBuildNumber}"),
              Text("is Blocked: ${ConfigApi.allUserBanned}"),
              Text("Block Reason: ${ConfigApi.allUserBannedReason}"),
              SizedBox(height: 25),
              Text(
                  "forceUpdateUnderBuildNumber: ${ConfigApi.forceUpdateUnderBuildNumber}"),
              Text(
                  "forceUpdateForBuildNumbers: ${ConfigApi.forceUpdateForBuildNumbers}"),
              Text("Information: ${ConfigApi.updateInformation}"),
              Text("Needs Update: ${ConfigApi.needUpdate}"),
              SizedBox(
                height: 15,
              ),
              MaterialButton(
                padding: EdgeInsets.all(0),
                onPressed: () async {
                  await VersionControlApi().checkVersion().then(
                        (value) => print(ConfigApi.allUserBanned),
                      );
                  setState(() {});
                },
                child: Container(
                  height: 40,
                  width: 150,
                  color: Colors.blue,
                  child: Center(
                    child: Text(
                      "Check Version",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
